import java.util.LinkedList;

public class LongStack {

    public static void main(String[] argum) {

        System.out.println(LongStack.interpret("5 6 + +"));

    }

    private LinkedList<Long> stack;

    LongStack() {
        stack = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        //https://www.geeksforgeeks.org/linkedlist-clone-method-in-java/
        LongStack cloneStack = new LongStack();
        cloneStack.stack = (LinkedList) stack.clone();
        return cloneStack;

    }

    public boolean stEmpty() {
        return stack.isEmpty();
    }

    public void push(long a) {
        stack.push(a);
    }

    public long pop() {
        if (stEmpty()) {
            throw new IndexOutOfBoundsException(
                    "Stack underflow");
        }
        return stack.pop();
    }

    public void op(String s) {
        if (stack.size() < 2) {
            throw new IndexOutOfBoundsException("Too short stack for" + s);
        }
        long element2 = pop();
        long element1 = pop();
        switch (s.charAt(0)) {
            case '+':
                push(element1 + element2);
                break;
            case '-':
                push(element1 - element2);
                break;
            case '*':
                push(element1 * element2);
                break;
            case '/':
                push(element1 / element2);
                break;
            default:
                throw new IllegalArgumentException("Illegal operation! " + s);
        }

    }

    public long tos() {
        if (stEmpty()) {
            throw new IndexOutOfBoundsException("Stack underflow");
        }
        return stack.peek();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof LongStack) {
            LinkedList<Long> list = ((LongStack) o).stack;
            if (stack.size() == list.size()) {
                for (int i = 0; i < stack.size(); i++)
                    if (!stack.get(i).equals(list.get(i)))
                        return false;
                return true;
            }
        }
        return false;

    }

    public void swap() {
        if (stack.size() < 2) {
            throw new RuntimeException("Not enough elements");
        }
        long first = stack.pop();
        long second = stack.pop();
        stack.push(first);
        stack.push(second);
    }

    public void rot() {
        if (stack.size() < 3) {
            throw new RuntimeException("Not enough elements");
        }
        long first = stack.pop();
        long second = stack.pop();
        long third = stack.pop();
        stack.push(second);
        stack.push(first);
        stack.push(third);
    }

    private boolean isNumber(String numberOrOperand) {
        try {
            Double.parseDouble(numberOrOperand);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = stack.size() - 1; i >= 0; i--) {
            str.append(stack.get(i)).append("/n");
        }
        return str.toString();
    }

    public static long interpret(String pol) {
        LongStack list = new LongStack();
        String[] items = pol.trim().split("\\s+");
        for (int i = 0; i < items.length; i++) {
            if (items[i].equals("SWAP")) {
                try {
                    list.swap();
                } catch (IndexOutOfBoundsException e) {
                    throw new RuntimeException("Not enough numbers in expression: " + pol);
                }
            } else if (items[i].equals("ROT")) {
                try {
                    list.rot();
                } catch (IndexOutOfBoundsException e) {
                    throw new RuntimeException("Not enough numbers in expression: " + pol);
                }
            } else if (list.isNumber(items[i])) {
                list.push(Long.parseLong(items[i]));
            } else {
                try {
                    list.op(items[i]);
                } catch (IndexOutOfBoundsException e) {
                    throw new RuntimeException("Not in enough numbers in expression: " + pol);
                } catch (IllegalArgumentException e) {
                    throw new RuntimeException(e + " .Expression: " + pol);
                }
            }
        }

        if (list.stack.size() != 1) {
            throw new RuntimeException("Too many numbers! Expression: " + pol);
        }
        return list.tos();
    }

}

